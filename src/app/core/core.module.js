define([
    'angular',
    'angularRoute',
    '../common/route/route.module'
], function(ng) {
    'use strict';

    var userInfoCore = ng.module('UserInfo.core', [
        'ngRoute',
        'UserInfo.route'
    ]);

    userInfoCore.config(configure);

    configure.$inject = ['$routeProvider', 'routehelperConfigProvider'];
    
    function configure ($routeProvider, routehelperConfigProvider) {
        // Configure the common route provider
        routehelperConfigProvider.config.$routeProvider = $routeProvider;
        routehelperConfigProvider.config.docTitle = 'GitHub User Info: ';
    }

    return userInfoCore;
});
