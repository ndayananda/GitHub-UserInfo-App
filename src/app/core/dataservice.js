define([
	'./core.module'
], function onReady (UserInfoCore, broadcastService) {

	'use strict';

	UserInfoCore.service('dataservice', dataservice);

    dataservice.$inject = ['$http', '$routeParams'];

	function dataservice($http, $routeParams) {
        var primePromise;
        var githubAPI = 'https://api.github.com/users';

        var service = {
            getUsers: getUsers,
            getUserDetails: getUserDetails
        };

        return service;

        function getUsers() {
            return $http.get(githubAPI)
                .then(getUsersComplete)
                .catch(function(message) {
                    console.error('XHR Failed for getUsers' + message);
                    $location.url('/');
                });

            function getUsersComplete(results, status, headers, config) {
                return results.data;
            }
        }

        function getUserDetails() {
            return $http.get(githubAPI+'/' + $routeParams.userID)
                .then(getUserDetailsComplete)
                .catch(function(message) {
                    console.error('XHR Failed for getUsers' + message);
                    $location.url('/');
                });

            function getUserDetailsComplete(results, status, headers, config) {
                return results.data;
            }
        }
	}
});
