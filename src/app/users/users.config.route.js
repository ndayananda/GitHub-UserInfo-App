define([
    'angular',
    '../common/route/routehelper'
], function onReady (ng) {

    'use strict';

    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    appRun.$inject = ['routehelper'];

    function getRoutes() {
        return [
            {
                url: '/',
                config: {
                    templateUrl: 'app/users/users.html',
                    controller: 'UsersCtrl',
                    controllerAs: 'vm',
                    title: 'Users'
                }
            }
        ];
    }

    return appRun;
})
