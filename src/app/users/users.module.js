define([
	'angular',
	'./users.config.route',
	'./users.ctrl'
], function onReady (ng, appRun, UsersCtrl) {

	'use strict';

    var users = ng.module('UserInfo.users', []);

    users.run(appRun);

	users.controller('UsersCtrl', UsersCtrl);

    return users;
});
