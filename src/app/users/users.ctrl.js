define([
	'./users.module',
	'../core/dataservice'
], function onReady (Users) {

	'use strict';

    UsersCtrl.$inject = ['dataservice'];

	function UsersCtrl(dataservice) {
		var vm = this; // vm is view model

        vm.users = [];

		vm.showLoader = true;

		activate();

        function activate() {
            return getUsers().then(function() {
                console.log('Users View Activated');
            });
        }

        function getUsers() {
            return dataservice.getUsers().then(function(data) {
                vm.users = data;
				vm.showLoader = false;
                return vm.users;
            });
        }
	}

    return UsersCtrl;
});
