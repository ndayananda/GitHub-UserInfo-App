;(function() {
	'use strict';

	/**
	 * In paths we set aliases for the libraries and plugins used, then we defined that angular should be shimmed
	 * A primary use of shim is with libraries that don't support AMD
	 */

	require.config({
		baseUrl: './app',
		paths:{
			require: '../../bower_components/requirejs/require',
			angular: '../../bower_components/angular/angular.min',
			angularRoute: '../../bower_components/angular-route/angular-route.min',
			domReady: '../../bower_components/requirejs-domready/domReady'
		},
		shim: {
			angular: {
				exports: 'angular'
			},
			angularRoute: {
				deps: ['angular'],
				exports: 'angularRoute'
			}
		},
		priority: [
			'angular'
		],
		deps: [
			'./angular.bootstrap'
		]
	});
})();
