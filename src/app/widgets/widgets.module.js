define([
	'angular',
	'./loaderWidget'
], function onReady (ng, loaderWidget) {

	'use strict';

	var userInfoWidgets = ng.module('UserInfo.widgets', []);

	userInfoWidgets.directive('loaderWidget', loaderWidget);

	return userInfoWidgets;
});
