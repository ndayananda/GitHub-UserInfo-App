define([
	'angular'
], function onReady (ng) {

	'use strict';

	function loaderWidget() {
		// Description:
        //  Creates a new loader and sets its options
        // Usage:
        // <loader-widget
        //      data-ng-show="<scope variable of controller>", if not provided then by default loader is visible
        //      loader-size="large", default is small
        //      is-colored="true"</div>, default is white
        //      enable-mask="true", default is false

		var directive =  {
				restrict: 'E',
				templateUrl: 'app/widgets/loader-widget.html',
				link: link,
				scope: {
					loaderSize: '@',
					isColored: '@',
					enableMask: '@'
				}
			};

		return directive;

		function link(scope, element, attrs) {

		}
	}

	return loaderWidget;
});
