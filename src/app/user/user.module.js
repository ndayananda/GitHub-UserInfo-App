define([
	'angular',
	'./user.config.route',
	'./user.ctrl'
], function onReady (ng, appRun, UserCtrl) {

	'use strict';

    var user = ng.module('UserInfo.user', []);

    user.run(appRun);

	user.controller('UserCtrl', UserCtrl);

    return user;
});
