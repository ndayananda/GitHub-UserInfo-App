define([
	'./user.module',
	'../core/dataservice'
], function onReady (Users) {

	'use strict';

    UserCtrl.$inject = ['dataservice'];

	function UserCtrl(dataservice) {
		var vm = this; // vm is view model

        vm.user = {};

		vm.numberFormatter = numberFormatter;

		vm.showLoader = true;

		activate();

        function activate() {
            getUserDetails().then(function() {
                console.log('User View Activated');
            });
        }

        function getUserDetails() {
            return dataservice.getUserDetails().then(function(data) {
                vm.user = data;
				vm.showLoader = false;
                return vm.user;
            });
        }

		function numberFormatter(num, digits) {
			if(!num) return '';

			var si = [
				{ value: 1E18, symbol: "E" },
				{ value: 1E15, symbol: "P" },
				{ value: 1E12, symbol: "T" },
				{ value: 1E9,  symbol: "G" },
				{ value: 1E6,  symbol: "M" },
				{ value: 1E3,  symbol: "k" }
			], rx = /\.0+$|(\.[0-9]*[1-9])0+$/, i;

			for (var i = 0; i < si.length; i++) {
				if (num >= si[i].value) {
					return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
				}
			}

			return num.toFixed(digits).replace(rx, "$1");
		}
	}

    return UserCtrl;
});
