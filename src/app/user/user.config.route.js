define([
    'angular',
    '../common/route/routehelper'
], function onReady (ng) {

    'use strict';

    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    appRun.$inject = ['routehelper'];

    function getRoutes() {
        return [
            {
                url: '/user/:userID',
                config: {
                    templateUrl: 'app/user/user.html',
                    controller: 'UserCtrl',
                    controllerAs: 'vm',
                    title: 'User Details'
                }
            }
        ];
    }

    return appRun;
})
