define([
    'angular',
    'angularRoute'
], function(ng) {
    'use strict';

    var userInfoRoute = ng.module('UserInfo.route', [
        'ngRoute'
    ]);

    return userInfoRoute;
});
