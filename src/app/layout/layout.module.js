define([
	'angular',
], function onReady (ng) {

	'use strict';

    var userInfoLayout = ng.module('UserInfo.layout', []);

    return userInfoLayout;
});
