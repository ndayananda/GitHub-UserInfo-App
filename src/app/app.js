define([
    'angular',
    './core/core.module',
    './layout/layout.module',
    './widgets/widgets.module',
    './users/users.module',
    './user/user.module'
], function onReady(ng) {
    'use strict';

    /*
     * This acts as a wrapper of the application
     * application is divided into modules
     * all the modules will be injected as dependencies
     * UserInfo acts as a superset
     */
    var userInfo = ng.module('UserInfo', [
        /*
         * Everybody has access to these.
         * We could place these under every feature area,
         * but this is easier to maintain.
         */
        'UserInfo.core',
        'UserInfo.layout',
        'UserInfo.widgets',

        /**
         * Other modules
         */
        'UserInfo.users',
        'UserInfo.user'
    ]);

    return userInfo;
});
