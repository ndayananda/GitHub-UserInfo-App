/**
 *  We’re bootstrapping angular manually now
 *  Bootstraps angular onto the window.document node
 *  Note that you don’t need ng-app in your html anymore.
 *  We use domReady RequireJS plugin to make sure that DOM is ready when we start the app
 */
define([
	'require',
	'angular',
	'domReady',
	'app'
], function onReady (require, ng, domReady) {
	'use strict';

	require(['domReady!', 'angular'], function(document, ng) {
		try {
			ng.bootstrap(document, ['UserInfo']);
		}
		catch(e) {
			console.error(e.stack || e.message || e);
		}
	})

});
