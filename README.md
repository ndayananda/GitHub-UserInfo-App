# GitHubUserInfo is a single page application built using AngularJS

This project is generated with nodejs, bower, grunt and angular

## Build & development

Run `grunt` for building, `grunt serve` for preview and 'grunt serve:dist' for preview from build directory.
